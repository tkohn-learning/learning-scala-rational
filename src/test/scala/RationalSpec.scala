import org.scalatest.FlatSpec

class RationalSpec extends FlatSpec {

  it should "produce IllegalArgumentException when denominator is zero" in {
    assertThrows[IllegalArgumentException] {
      new Rational(1, 0)
    }
  }

  "Sum of 1/2 and 2/3" should "be 7/6" in {
    val oneHalf = new Rational(1, 2)
    val twoThird = new Rational(2, 3)
    val expected = new Rational(7, 6)
    assert(oneHalf + twoThird == expected)
  }

  "Sum of 1/2 and 3" should "be 7/2" in {
    val oneHalf = new Rational(1, 2)
    val expected = new Rational(7, 2)
    assert(oneHalf + 3 == expected)
  }

  "Substract 1/4 from 1/3" should "be 1/12" in {
    val oneQuarter = new Rational(1, 4)
    val oneThree = new Rational(1, 3)
    val expected = new Rational(1, 12)
    assert(oneThree - oneQuarter == expected)
  }

  "Substract 5 from 1/2" should "be -9/2" in {
    val oneHalf = new Rational(1, 2)
    val expected = new Rational(-9, 2)
    assert(oneHalf - 5 == expected)
  }

  "Multiply 2/3 with 1/2" should "be 1/3" in {
    val twoThird = new Rational(2, 3)
    val twoHalf = new Rational(1, 2)
    val expected = new Rational(1, 3)
    assert(twoThird * twoHalf == expected)
  }

  "Multiply 2/3 with 7" should "be 14/3" in {
    val twoThird = new Rational(2, 3)
    val expected = new Rational(14, 3)
    assert(twoThird * 7 == expected)
  }

  "Divide 2/3 with 5/2" should "be 4/15" in {
    val twoThird = new Rational(2, 3)
    val fiveHalf = new Rational(5, 2)
    val expected = new Rational(4, 15)
    assert(twoThird / fiveHalf == expected)
  }

  "Divide 7/8 with 23" should "be 7/184" in {
    val sevenOverEight = new Rational(7, 8)
    val expected = new Rational(7, 184)
    assert(sevenOverEight / 23 == expected)
  }

  "Sum of 1/2 and 1/2" should "be 1 for toString" in {
    val oneHalf = new Rational(1, 2)
    val result = oneHalf + oneHalf
    assert(result.toString == "1")
  }

}
