class Rational(n: Int, d: Int) {

  require(d != 0, "The denominator can not be zero.")
  private val g = gcd(n.abs, d.abs)
  val numerator: Int = (if (d < 0) -n else n) / g
  val denominator: Int = d.abs / g

  def this(n: Int) = this(n, 1)

  def +(that: Rational): Rational =
    new Rational(
      numerator * that.denominator + that.numerator * denominator,
      denominator * that.denominator
    )
  def +(i: Int): Rational =
    new Rational(numerator + i * denominator, denominator)

  def -(that: Rational): Rational =
    new Rational(
      numerator * that.denominator - that.numerator * denominator,
      denominator * that.denominator
    )
  def -(i: Int): Rational =
    new Rational(numerator - i * denominator, denominator)

  def *(that: Rational): Rational =
    new Rational(numerator * that.numerator, denominator * that.denominator)
  def *(i: Int): Rational = new Rational(numerator * i, denominator)

  def /(that: Rational): Rational =
    new Rational(numerator * that.denominator, denominator * that.numerator)
  def /(i: Int): Rational = new Rational(numerator, denominator * i)

  override def toString: String =
    if (denominator == 1) numerator.toString
    else numerator + "/" + denominator

  override def hashCode(): Int = (numerator, denominator).##

  def canEqual(other: Any): Boolean = other.isInstanceOf[Rational]

  override def equals(other: Any): Boolean =
    other match {
      case that: Rational =>
        (that canEqual this) &&
          numerator == that.numerator &&
          denominator == that.denominator
      case _ => false
    }

  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
}
