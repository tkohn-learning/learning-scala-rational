scalaVersion := "2.13.1"


name := "learning-scala-rational"
organization := "dev.kohn"
version := "1.0"

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"

logBuffered in Test := true